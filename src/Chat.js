import React from 'react';
import Header from "./Header";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import {getCurrentTime} from './utils/getCurrentTime';


class Chat extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            users: this.props.data,
            currentUser: 'Andrew'
        }
    }

    getHeaderInfo(users) {
        let usersCount = new Set();
            users.forEach(user => {
               usersCount.add(user.user);
            });
        let messageCount = users.length;
            return {
                users: usersCount.size,
                messages: messageCount,
                lastMessage: users[users.length - 1].created_at
            }
    }

    sendMessage = (text) => {
        let newMessage = {
          id: (parseInt(this.state.users[this.state.users.length - 1].id) + 10).toString(),
          user: this.state.currentUser,
          created_at: getCurrentTime(),
          message: text
        };
        console.log(newMessage);
        this.setState({
           users: [...this.state.users, newMessage]
        });
    };

    editMessage = (id, message) => {
        let usersEditMessage = this.state.users;
        usersEditMessage.forEach(user => {
           if (user.id === id) {
               user.message = message;
           }
        });
        this.setState({
            users: usersEditMessage
        });
    };

    deleteMessage = (id) => {
        let userDeleteId;
        let usersDeleteMessage = this.state.users;
        usersDeleteMessage.forEach((user, index) => {
            if (user.id === id){
                userDeleteId = index;
            }
        });
        usersDeleteMessage.splice(userDeleteId,1);
        this.setState({
           users:  usersDeleteMessage
        });
    };

    render() {
        let headerInfo = this.getHeaderInfo(this.state.users);
        return (
            <div className="Chat">
                <Header headerInfo = {headerInfo} />
                <MessageList users = {this.state.users} currentUser = {this.state.currentUser} editMessage={this.editMessage} deleteMessage={this.deleteMessage} />
                <MessageInput sendMessage={this.sendMessage} />
            </div>
        );
    }
}

export default Chat;