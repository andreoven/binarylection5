import React from 'react';
import Chat from './Chat';
import Spinner from './Spinner';

class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isLoaded: false,
            data: null
        }
    }

    componentDidMount() {
        fetch('https://api.myjson.com/bins/1hiqin')
        .then(response => response.json())
        .then(data => this.setState({
            isLoaded: true,
            data: data
        }));
    }

    render() {
        if (this.state.isLoaded) {
            return (<Chat data = {this.state.data} />);
        } else {
            return (<Spinner/>);
        }
    }
}

export default App;