import React from 'react'
import Loader from 'react-loader-spinner'

function Spinner() {
    return (
        <div className="Loader">
            <Loader
                type="Oval"
                color="#808080"
                height="100"
                width="100"
            />
        </div>
    );
}


export default Spinner;