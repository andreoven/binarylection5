import React from 'react';
import Message from './Message';

class MessageList extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
        const users = this.props.users;
        let date = (users[0].created_at).toString().split(" ")[0];
        console.log(users);
        const messageItems = users.map((user, index) => {
            if (date === (user.created_at).toString().split(" ")[0]) {
                return (
                    <li key={user.id}>
                        <Message user = {user} isSeparator = {false} currentUser = {this.props.currentUser} editMessage={this.props.editMessage} deleteMessage={this.props.deleteMessage} />
                    </li>
                )
            } else {
                let dateSeparator = date;
                date = (user.created_at).toString().split(" ")[0];
                return (
                    <li key={user.id}>
                        <Message user = {user} isSeparator={dateSeparator} currentUser = {this.props.currentUser} editMessage={this.props.editMessage} deleteMessage={this.props.deleteMessage} />
                    </li>
                )
            }
        });

        return (
            <div className="MessageList">
                {messageItems}
            </div>
        );
    }
}

export default MessageList;