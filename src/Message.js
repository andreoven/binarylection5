import React from 'react';
import Line from "./Line";

class Message extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            isEdit: false,
            like: 'Like'
        }
    }

    onChange(e) {
        this.setState({
            text: e.target.value
        })
    }

    onEdit(message) {
        this.setState({
            text: message,
            isEdit: !this.state.isEdit
        })
    }

    closeInput(id, message) {
        const {editMessage} = this.props;
        editMessage(id, message);
        this.setState({isEdit: !this.state.isEdit})

    }

    likeMessage(){
        if (this.state.like === 'Like') {
            this.setState({
                like: 'Liked'
            })
        } else {
            this.setState({
                like: 'Like'
            })
        }
    }

    render() {
        const {user, isSeparator, currentUser, deleteMessage} = this.props;
        if (user.user === currentUser) {
            if (this.state.isEdit) {
                return (
                    <div className="message">
                        <Line separator = {isSeparator} />
                        <div className="message-main">
                            <div className="message-image">
                                <img src={user.avatar} alt=""/>
                            </div>
                            <div className="message-data">
                                <div className="message-user">
                                    <p>Name: {user.user}</p>
                                    <p>Date: {user.created_at}</p>
                                </div>
                                <div className="message-text">
                                    <input type="text" value={ this.state.text } onChange={ (e) => this.onChange(e) }/>
                                    <p onClick={() => this.closeInput(user.id, this.state.text)}>Save</p>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className="message">
                        <Line separator = {isSeparator} />
                        <div className="message-main">
                            <div className="message-image">
                                <img src={user.avatar} alt=""/>
                            </div>
                            <div className="message-data">
                                <div className="message-user">
                                    <p>Name: {user.user}</p>
                                    <p>Date: {user.created_at}</p>
                                </div>
                                <div className="message-text">
                                    <p>Message: {user.message}</p>
                                    <div className="message-edit">
                                        <p onClick={() => this.onEdit(user.message)}>Edit</p>
                                        <p onClick={() => this.likeMessage()}>{this.state.like}</p>
                                        <p onClick={() => deleteMessage(user.id)}> Delete</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
        }  else {
            return (
                <div className="message">
                    <Line separator = {isSeparator} />
                    <div className="message-main">
                        <div className="message-image">
                            <img src={user.avatar} alt=""/>
                        </div>
                        <div className="message-data">
                            <div className="message-user">
                                <p>Name: {user.user}</p>
                                <p>Date: {user.created_at}</p>
                            </div>
                            <div className="message-text">
                                <p>Message: {user.message}</p>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default Message;