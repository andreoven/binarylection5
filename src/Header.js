import React from 'react';


class Header extends React.Component {
    constructor(props){
        super(props);

    }

    render() {
        let date = (new Date(this.props.headerInfo.lastMessage)).toString();
        let dateArray = date.split(" ");
        return (
            <div className="Header">
                <p>My Chat</p>
                <p>{this.props.headerInfo.users} Participants</p>
                <p>{this.props.headerInfo.messages} Messages</p>
                <p>Last message at: {dateArray[0]}, {dateArray[1]} {dateArray[2]}</p>
            </div>
        );
    }
}

export default Header;